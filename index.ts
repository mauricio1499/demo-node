import express, { Express, Request, Response } from 'express';
import dotenv from 'dotenv';
var cors = require('cors')

dotenv.config();

const app: Express = express();
const port = process.env.PORT || 3000;
var cors = require('cors')

app.use(cors())

app.get('/security', (req: Request, res: Response) => {
  res.send(`Express + TypeScript Server ${process.env.NAME??'--'}`);
});

app.get('/security/2', (req: Request, res: Response) => {
  res.send(`Express + TypeScript Server ${process.env.TEST_2??'--'}`);
});

app.get('/hola-mundo', (req: Request, res: Response) => {
  res.send( `Express + TypeScript Server hola mundo chg - ${process.env.TEST??'-'}`);
});

app.listen(port, () => {
  console.log(`⚡️[server]: Server is running at http://localhost:${port}`);
});